﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buchfuehrung.DOMAIN
{
    class Buchungsstapel
    {


        private List<Buchung> Buchungen = new List<Buchung>();
        private Buchungsstapel (List<Buchung> Buchungen)
        {
            List<bool> BuchungenSindAusgeglichen = new List<bool>();
            foreach (Buchung buchung in Buchungen)
            {
                BuchungenSindAusgeglichen.Add(Buchung.PrüfeObBuchungAusgeglichen(buchung.getPositionen()));
            }
            if(BuchungenSindAusgeglichen.Any(val => val == false))
                throw new System.Exception("Positionen sind nicht ausgeglichen!");
            this.Buchungen = Buchungen;
        }

        public static Buchungsstapel ErstelleBuchungsstapel(List<Buchung> buchungen)

        {
            Buchungsstapel buchungsstapel = new Buchungsstapel(buchungen);
            return buchungsstapel;
        }
    }
}
