﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buchfuehrung.DOMAIN
{
    internal class Saldieren
    {
        public static Betrag saldiere(Betrag lhs, Betrag rhs)
        {
            if (lhs is Sollbetrag)
            {
                if (rhs is Sollbetrag)
                {
                    return saldiere(lhs as Sollbetrag, rhs as Sollbetrag);
                } else if (rhs is Habenbetrag)
                {
                    return saldiere(lhs as Sollbetrag, rhs as Habenbetrag);
                } else
                {
                    return lhs;
                }
            } else if (lhs is Habenbetrag)
            {
                Habenbetrag haben = lhs as Habenbetrag;
                if (rhs is Sollbetrag)
                {
                    return saldiere(haben, rhs as Sollbetrag);
                } else if (rhs is Habenbetrag)
                {
                    return saldiere(haben, rhs as Habenbetrag);
                } else
                {
                    return lhs;
                }
            } else
            {
                return rhs;
            }
        }

        private static Betrag saldiere(Sollbetrag lhs, Sollbetrag rhs)
        {
            return Betrag.SollInCent(lhs.GetBetragInCent() + rhs.GetBetragInCent());
        }
        private static Betrag saldiere(Habenbetrag lhs, Habenbetrag rhs)
        {
            return Betrag.HabenInCent(lhs.GetBetragInCent() + rhs.GetBetragInCent());
        }
        private static Betrag saldiere(Sollbetrag lhs, Habenbetrag rhs)
        {
            if (lhs.GetBetragInCent() == rhs.GetBetragInCent())
            {
                return Betrag.Ausgeglichen();
            }
            else if (lhs.GetBetragInCent() > rhs.GetBetragInCent())
            {
                return Betrag.SollInCent(lhs.GetBetragInCent() - rhs.GetBetragInCent());
            }
            else
            {
                return Betrag.HabenInCent(rhs.GetBetragInCent() - lhs.GetBetragInCent());
            }
        }
        private static Betrag saldiere(Habenbetrag lhs, Sollbetrag rhs)
        {
            return saldiere(rhs, lhs);
        }
        private static Betrag saldiere(Betrag betrag, AusgeglichenerBetrag ausgeglichen)
        {
            return betrag;
        }
        private static Betrag saldiere(AusgeglichenerBetrag ausgeglichen, Betrag betrag)
        {
            return saldiere(betrag, ausgeglichen);
        }
        private static Betrag saldiere(AusgeglichenerBetrag lhs, AusgeglichenerBetrag rhs)
        {
            return lhs;
        }
    }
}