﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buchfuehrung.DOMAIN
{
    internal class Grundbuch
    {
        private Buchungssystem buchungssystem;

        public Grundbuch(Buchungssystem buchungssystem)
        {
            this.buchungssystem = buchungssystem;
        }

        public IList<Buchung> GetAlleBuchungen()
        {
            return buchungssystem.Buchungen.AsReadOnly();
        }

        public Buchung GetLetzteBuchung()
        {
            List<Buchung> buchungen = buchungssystem.Buchungen;

            return (buchungen.Count > 0) ? buchungen[buchungen.Count - 1] : null;
        }

        public List<Buchung> GetBuchungenMitKonto(Konto konto)
        {
            List<Buchung> alleBuchungen = buchungssystem.Buchungen;

            return alleBuchungen.Where(buchung => 
            {
                return buchung.getPositionen().Any(position =>
                {
                    return position.Key.Equals(konto);
                });
            }).ToList();
        }
    }
}
