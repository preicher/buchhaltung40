﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buchfuehrung.DOMAIN
{
    internal class Buchung
    {
        private int Belegnummer;
        private Dictionary<Konto, Betrag> Positionen;

        public Buchung(Dictionary<Konto, Betrag> Positionen)
        {
            if( Positionen.Count < 2 || Positionen.Count > 5)
                throw new System.Exception("Anzahl der Positionen sind nicht ausgeglichen!");
            if(!PrüfeObBuchungAusgeglichen(Positionen))
                throw new System.Exception("Positionen sind nicht ausgeglichen!");
            this.Positionen = Positionen;
        }

        public static Buchung LegeBuchungAn(Dictionary<Konto, Betrag> Positionen)
        {
            Buchung buchung = new Buchung(Positionen);
            return buchung;
        }
        public Dictionary<Konto, Betrag> getPositionen()
        {
            return Positionen;
        }

        public static bool PrüfeObBuchungAusgeglichen(Dictionary<Konto, Betrag> Positionen)
        {
            Betrag betrag = null;
            foreach (KeyValuePair<Konto, Betrag> entry in Positionen)
            {
                betrag = Saldieren.saldiere(betrag, entry.Value);
                //tmpPositonen.Add(entry.Key, entry.Value);
            }
            if (betrag != null && !betrag.istAusgeglichen())
            {
                return false;
                
            }
            return true;

        }
    }
}