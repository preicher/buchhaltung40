﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buchfuehrung.DOMAIN
{
    internal class Buchungssystem
    {
        private Dictionary<Konto, Betrag> Kontostaende { get; set; }
        internal List<Buchung> Buchungen { get; private set; }

        public Buchungssystem()
        {
            Kontostaende = new Dictionary<Konto, Betrag>();
            Buchungen = new List<Buchung>();
        }

        public Betrag Kontostand(Konto konto)
        {
            if (Kontostaende.Keys.Contains(konto))
            {
                return Kontostaende[konto];
            }

            return Betrag.Ausgeglichen();
        }

        public void AddBuchung(Buchung buchung)
        {
            Buchungen.Add(buchung);
            foreach (var pos in buchung.getPositionen())
            {
                Buche(pos.Key, pos.Value);
            }
        }

        private void Buche(Konto konto, Betrag betrag)
        {
            Kontostaende[konto] = Saldieren.saldiere(Kontostand(konto), betrag);
        }
    }
}