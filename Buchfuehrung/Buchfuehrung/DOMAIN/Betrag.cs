﻿using System;

namespace Buchfuehrung.DOMAIN
{
    abstract class Betrag
    {
        public static Betrag SollInCent(int soll)
        {
            return new Sollbetrag(soll);
        }

        public static Betrag HabenInCent(int haben)
        {
            return new Habenbetrag(haben);
        }

        public static Betrag Ausgeglichen()
        {
            return new AusgeglichenerBetrag();
        }
        public virtual bool istAusgeglichen()
        {
            return false;
        }
        public abstract int GetBetragInCent();

    }

    class Sollbetrag : Betrag
    {
        private readonly int _sollBetragInCent;

        public Sollbetrag(int soll)
        {
            if (soll == 0)
            {
                throw new ArgumentNullException();
            }
            else
            {
                _sollBetragInCent = soll;
            } 
        }

        public override int GetBetragInCent()
        {
            return _sollBetragInCent;
        }

        public override bool istAusgeglichen()
        {
            return false;
        }

        public override bool Equals(object obj)
        {
            if (obj is Sollbetrag) 
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    class Habenbetrag : Betrag
    {
        private readonly int _habenBetragInCent;

        public Habenbetrag(int soll)
        {
            if (soll == 0)
            {
                throw new ArgumentNullException();
            }
            else
            {
                _habenBetragInCent = soll;
            }    
        }

        public override int GetBetragInCent()
        {
            return _habenBetragInCent;
        }

        public override bool istAusgeglichen()
        {
            return false;
        }

        public override bool Equals(object obj)
        {
            if (obj is Habenbetrag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    class AusgeglichenerBetrag : Betrag
    {
        public AusgeglichenerBetrag() {}

        public override int GetBetragInCent()
        {
            return 0;
        }

        public override bool istAusgeglichen()
        {
            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj is AusgeglichenerBetrag)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
