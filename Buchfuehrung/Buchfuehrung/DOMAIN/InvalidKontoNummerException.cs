﻿using System;
using System.Runtime.Serialization;

namespace Buchfuehrung.DOMAIN
{
    [Serializable]
    internal class InvalidKontoNummerException : Exception
    {
        public InvalidKontoNummerException()
        {
        }

        public InvalidKontoNummerException(string message) : base(message)
        {
        }

        public InvalidKontoNummerException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidKontoNummerException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}