﻿namespace Buchfuehrung.DOMAIN
{
    public class Konto
    {
        public string KontoNummer { get;}

        public Konto(string kontoNummer)
        {
            if(kontoNummer == null || kontoNummer.Length == 0)
            {
                throw new InvalidKontoNummerException("The given Kontonummer is not valid: " + kontoNummer);
            }
            KontoNummer = kontoNummer;
        }

        public override bool Equals(object obj)
        {
            if (obj is Konto)
            {
                return KontoNummer.Equals((obj as Konto).KontoNummer);
            }
            else
            {
                return false;
            }    
        }

        public override int GetHashCode()
        {
            return KontoNummer.GetHashCode();
        }

        public override string ToString()
        {
            return "Konto Nr: " + KontoNummer;
        }
    }
}
