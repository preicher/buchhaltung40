﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Buchfuehrung.Adapter;

namespace Buchfuehrung.Tests
{ 
    [TestFixture]
    class BuchungssystemUIPrimitiveTest
    {

        [Test]
        public void printMessageTest()
        {
            string message = "Test message.";
            BuchungssystemUIPrimitive ui = new BuchungssystemUIPrimitive();
            ui.printMessage(message);
            Assert.AreEqual(message, ui.getLastMessage());
        }

        [Test]
        public void readStringTest()
        {
            string type = "Type";
            string testString = "Test string";
            BuchungssystemUIPrimitive ui = new BuchungssystemUIPrimitive();
            ui.setString(type, testString);
            string read = ui.readString(type);
            Assert.AreEqual(testString, read);
        }
    }
}
