﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Buchfuehrung.DOMAIN;
using Buchfuehrung.Adapter;

namespace Buchfuehrung.Tests
{
    [TestFixture]
    public class BuchungssystemErstellungUITest
    {
        [Test]
        public void PrintMessageTest()
        {
            string message = "Buchungssatz testmessage";
            BuchungssystemErstellungUI ui = new BuchungssystemErstellungUI();
            ui.printMessage(message);
            Assert.AreEqual(message, ui.getLastMessage());
        }
        [Test]
        public void readNameBuchungssytem()
        {
            string nameBuchungssystem = "TestKontoName";
            BuchungssystemErstellungUI ui = new BuchungssystemErstellungUI();
            ui.setNameBuchungssystem(nameBuchungssystem);
            Assert.AreEqual(nameBuchungssystem, ui.readNameBuchungssystem());
        }
        [Test]
        public void readNameKontorahmen()
        {
            string nameKontorahmen = "123456";
            BuchungssystemErstellungUI ui = new BuchungssystemErstellungUI();
            ui.setNameKontorahmen(nameKontorahmen);
            Assert.AreEqual(nameKontorahmen, ui.readNameKontorahmen());
        }
    }
}