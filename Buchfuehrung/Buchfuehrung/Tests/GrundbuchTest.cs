﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buchfuehrung.DOMAIN;
using NUnit.Framework;

namespace Buchfuehrung.Tests
{
    [TestFixture]
    public class GrundbuchTest
    {
        private Buchungssystem createBuchungsystemMitDreiBuchungen()
        {
            Buchungssystem buchungssystem = new Buchungssystem();
            Konto konto1 = new Konto("34839");
            Konto konto2 = new Konto("234212");
            Konto konto3 = new Konto("192");
            Betrag betrag2 = Betrag.SollInCent(30);
            Betrag betrag1 = Betrag.HabenInCent(30);
            Betrag betrag3 = Betrag.SollInCent(78);
            Betrag betrag4 = Betrag.HabenInCent(78);
            Betrag betrag5 = Betrag.SollInCent(25);
            Betrag betrag6 = Betrag.HabenInCent(25);
            Dictionary<Konto, Betrag> positionen1 = new Dictionary<Konto, Betrag>()
            {
                [konto1] = betrag1,
                [konto2] = betrag2
            };
            Dictionary<Konto, Betrag> positionen2 = new Dictionary<Konto, Betrag>()
            {
                [konto1] = betrag3,
                [konto2] = betrag4
            };
            Dictionary<Konto, Betrag> positionen3 = new Dictionary<Konto, Betrag>()
            {
                [konto1] = betrag5,
                [konto3] = betrag6
            };

            Buchung buchung1 = Buchung.LegeBuchungAn(positionen1);
            Buchung buchung2 = Buchung.LegeBuchungAn(positionen2);
            Buchung buchung3 = Buchung.LegeBuchungAn(positionen3);

            // Add bookings
            buchungssystem.AddBuchung(buchung1);
            buchungssystem.AddBuchung(buchung2);
            buchungssystem.AddBuchung(buchung3);

            return buchungssystem;
        }

        [Test]
        public void CreateGrundbuch()
        {
            Buchungssystem buchungssystem = new Buchungssystem();
            Grundbuch grundbuch = new Grundbuch(buchungssystem);

            Assert.IsNotNull(grundbuch);
        }

        [Test]
        public void Grundbuch_GetLetzteEintrag_WithEmptyBuchungen_ReturnsNull()
        {
            // Empty buchungsystem
            Buchungssystem buchungssystem = new Buchungssystem();
            Grundbuch grundbuch = new Grundbuch(buchungssystem);

            Assert.AreEqual(grundbuch.GetLetzteBuchung(), null);
        }

        [Test]
        public void Grundbuch_GetLetzteEintrag_WithNonEmptyBuchungen_ReturnsLetzteEintrag()
        {
            Buchungssystem buchungssystem = createBuchungsystemMitDreiBuchungen();

            List<Buchung> alleBuchungen = buchungssystem.Buchungen;
            Grundbuch grundbuch = new Grundbuch(buchungssystem);

            Assert.AreEqual(grundbuch.GetLetzteBuchung(), alleBuchungen[alleBuchungen.Count - 1]);
        }

        [Test]
        public void Grundbuch_GetAlleBuchungen_ReturnsAlleEintraege()
        {
            Buchungssystem buchungssystem = createBuchungsystemMitDreiBuchungen();
            List<Buchung> alleBuchungen = buchungssystem.Buchungen;
            Grundbuch grundbuch = new Grundbuch(buchungssystem);

            Assert.AreEqual(grundbuch.GetAlleBuchungen(), alleBuchungen);
        }

        [Test]
        public void Grundbuch_GetBuchungenMitKontoNummer_ReturnsBuchungenMitGivenKontoNummer()
        {
            Buchungssystem buchungssystem = createBuchungsystemMitDreiBuchungen();
            List<Buchung> alleBuchungen = buchungssystem.Buchungen;
            Grundbuch grundbuch = new Grundbuch(buchungssystem);
            Konto konto = new Konto("192");

            // Test if the returned buchungen contains only buchungen from the given konto.
            Assert.AreEqual(grundbuch.GetBuchungenMitKonto(konto).Count, 1);
        }
    }
}