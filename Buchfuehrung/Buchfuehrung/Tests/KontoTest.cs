﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buchfuehrung.DOMAIN;
using NUnit.Framework;

namespace Buchfuehrung.Tests
{
    [TestFixture]
    public class KontoTest
    {
        readonly string validKontoNummer = "123123";

        [Test]
        public void CreateKonto_WithSixLettersKontoNummer_ReturnsKonto()
        {
            Konto konto = new Konto(validKontoNummer);

            Assert.IsNotNull(konto);
        }

        [Test]
        public void CreateKonto_WithEmptyKontoNummer_ThrowsException()
        {
            Assert.Throws<InvalidKontoNummerException>(delegate { new Konto(""); });
        }

        [Test]
        public void CreateKonto_WithNullKontoNummer_ThrowsException()
        {
            Assert.Throws<InvalidKontoNummerException>(delegate { new Konto(null); });
        }

        [Test]
        public void KontoEquals_WithEqualKonto_ReturnsTrue()
        {
            Konto konto1 = new Konto(validKontoNummer);
            Konto konto2 = new Konto(validKontoNummer);
            Konto konto3 = new Konto(validKontoNummer + "!");

            Assert.IsTrue(konto1.Equals(konto1));
            Assert.IsTrue(konto1.Equals(konto2));
            Assert.IsFalse(konto1.Equals(konto3));
            Assert.AreEqual(konto1, konto2);
            Assert.AreEqual(konto2, konto2);
        }

        [Test]
        public void KontoEquals_WithInEqualKonto_ReturnsFalse()
        {
            Konto konto1 = new Konto(validKontoNummer);
            Konto konto2 = new Konto(validKontoNummer + "!");

            Assert.IsFalse(konto1.Equals(konto2));
            Assert.AreNotEqual(konto1, konto2);
        }

        [Test]
        public void GetHashCode_ReturnsHashCodeOfKontoNummer()
        {
            Konto konto = new Konto(validKontoNummer);
            Assert.AreEqual(validKontoNummer.GetHashCode(), konto.GetHashCode());
        }
    }
}