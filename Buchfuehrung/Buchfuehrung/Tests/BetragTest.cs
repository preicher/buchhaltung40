﻿using Buchfuehrung.DOMAIN;
using NUnit.Framework;

namespace Buchfuehrung.Tests
{
    [TestFixture]
    public class BetragTest
    {  
        [Test]
        public void SollBetrag()
        {
            int betragInCent = 2;
            Betrag betr = Betrag.SollInCent(betragInCent);
            Assert.AreEqual(new Sollbetrag(betragInCent), betr);
        }

        [Test]
        public void HabenBetrag()
        {
            int betragInCent = 2;
            Betrag betr = Betrag.HabenInCent(betragInCent);
            Assert.AreEqual(new Habenbetrag(betragInCent), betr);
        }

        [Test]
        public void AusgeglichenerBetrag()
        {
            Betrag betr = Betrag.Ausgeglichen();
            Assert.AreEqual(new AusgeglichenerBetrag(), betr);
        }
    }
}