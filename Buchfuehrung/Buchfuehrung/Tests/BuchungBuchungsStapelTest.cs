﻿using Buchfuehrung.Anwendungslayer;
using Buchfuehrung.DOMAIN;
using Buchfuehrung.Ports;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buchfuehrung.Tests
{
    [TestFixture]
    class BuchungBuchungsStapelTest
    {
        private IBuchungBuchungsStapelRepoPort buchungBuchungsStapelRepoPort;       
        private BuchungBuchungsStapel buchungBuchungsStapel;
        private string buchungsStapelName = "stapel1";

        [SetUp]
        public void SetUp()
        {
            buchungBuchungsStapelRepoPort = Substitute.For<IBuchungBuchungsStapelRepoPort>();
            buchungBuchungsStapelRepoPort.LoadBuchungsSystem(Arg.Any<string>()).Returns(createBuchungsystemMitDreiBuchungen());
            buchungBuchungsStapel = new BuchungBuchungsStapel(buchungBuchungsStapelRepoPort);
        }

        private Buchungssystem createBuchungsystemMitDreiBuchungen()
        {
            Buchungssystem buchungssystem = new Buchungssystem();
            Konto konto1 = new Konto("34839");
            Konto konto2 = new Konto("234212");
            Konto konto3 = new Konto("192");
            Betrag betrag2 = Betrag.SollInCent(30);
            Betrag betrag1 = Betrag.HabenInCent(30);
            Betrag betrag3 = Betrag.SollInCent(78);
            Betrag betrag4 = Betrag.HabenInCent(78);
            Betrag betrag5 = Betrag.SollInCent(25);
            Betrag betrag6 = Betrag.HabenInCent(25);
            Dictionary<Konto, Betrag> positionen1 = new Dictionary<Konto, Betrag>()
            {
                [konto1] = betrag1,
                [konto2] = betrag2
            };
            Dictionary<Konto, Betrag> positionen2 = new Dictionary<Konto, Betrag>()
            {
                [konto1] = betrag3,
                [konto2] = betrag4
            };
            Dictionary<Konto, Betrag> positionen3 = new Dictionary<Konto, Betrag>()
            {
                [konto1] = betrag5,
                [konto3] = betrag6
            };

            Buchung buchung1 = Buchung.LegeBuchungAn(positionen1);
            Buchung buchung2 = Buchung.LegeBuchungAn(positionen2);
            Buchung buchung3 = Buchung.LegeBuchungAn(positionen3);

            // Add bookings
            buchungssystem.AddBuchung(buchung1);
            buchungssystem.AddBuchung(buchung2);
            buchungssystem.AddBuchung(buchung3);

            return buchungssystem;
        }

        [Test]
        public void CreateBuchungsStapel_ReturnsBuchungsStapel()
        {
            Buchungsstapel buchungsStapel = buchungBuchungsStapel.CreateBuchungsStapel(buchungsStapelName);

            Assert.IsNotNull(buchungsStapel);
        }

        [Test]
        public void LoadBuchungsStapel_ReturnsBuchungsStapel()
        {
            Buchungsstapel expectedBuchungsStapel = null; // = new Buchungsstapel();
            buchungBuchungsStapelRepoPort.LoadBuchungsStapel(Arg.Any<string>()).Returns(expectedBuchungsStapel);
            Buchungsstapel actualBuchungsStapel = buchungBuchungsStapel.LoadBuchungsStapel(buchungsStapelName);

            Assert.AreEqual(expectedBuchungsStapel, actualBuchungsStapel);
        }

        [Test]
        public void SaveBuchungsStapel_CallsBspRepoSave()
        {
            Buchungsstapel expectedBuchungsStapel; // = new Buchungsstapel();
            buchungBuchungsStapel.SaveBuchungsStapel(buchungsStapelName);

            buchungBuchungsStapelRepoPort.Received(1).SaveBuchungsStapel(Arg.Any<string>());
        }

        [Test]
        public void BucheBuchungsStapel_BuchtBuchungsStapelInBuchungsSystem()
        {
            Buchungsstapel expectedBuchungsStapel; // = new Buchungsstapel();
            buchungBuchungsStapel.CreateBuchungsStapel(buchungsStapelName);
            string message;
            bool success = buchungBuchungsStapel.BucheBuchungsStapel(buchungsStapelName, out message);
        

            Assert.IsTrue(success);

        }
    }
}
