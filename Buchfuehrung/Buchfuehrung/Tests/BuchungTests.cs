﻿using System;
using NUnit.Framework;
using Buchfuehrung.DOMAIN;
using System.Collections.Generic;

namespace Buchfuehrung.Tests
{
    [TestFixture]
    public class BuchungTests
    {
        [Test]
        public void TesteInitialisierenEinerBuchungFürLeeresDictionary()
        {
            Dictionary <Konto, Betrag> Positionen = new Dictionary<Konto, Betrag>();
            Assert.Throws<Exception>(delegate { new Buchung(Positionen); }, "Anzahl der Positionen sind nicht ausgeglichen!");
        }

        [Test]
        public void TesteInitialisierenEinerBuchungFürAusgeglichenePositionenImDictionary()
        {
            Dictionary<Konto, Betrag> Positionen = new Dictionary<Konto, Betrag>();
            Konto konto1 = new Konto("12345");
            Konto konto2 = new Konto("67891");
            Sollbetrag betrag1 = new Sollbetrag(5);
            Habenbetrag betrag2 = new Habenbetrag(5);
            Positionen.Add(konto1, betrag1);
            Positionen.Add(konto2, betrag2);
            Buchung buchung = new Buchung(Positionen);

            Assert.IsNotNull(buchung);
        }
        [Test]
        public void TesteInitialisierenEinerBuchungFürNichtAusgeglichenePositionenImDictionary()
        {

            Dictionary<Konto, Betrag> Positionen = new Dictionary<Konto, Betrag>();
            Konto konto1 = new Konto("12345");
            Konto konto2 = new Konto("67891");
            Sollbetrag betrag1 = new Sollbetrag(3);
            Habenbetrag betrag2 = new Habenbetrag(5);
            Positionen.Add(konto1, betrag1);
            Positionen.Add(konto2, betrag2);
            Assert.Throws<Exception>(delegate { new Buchung(Positionen); }, "Anzahl der Positionen sind nicht ausgeglichen!");

            //Assert.Throws < I
            //Assert.IsNotNull(buchung);
        }
    }
}