﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buchfuehrung.DOMAIN;
using NUnit.Framework;

namespace Buchfuehrung.Tests
{
    [TestFixture]
    public class BuchungsstapelTests
    {
        

        [Test]
        public void TesteInitierenEinesBuchungsstapel()
        {
            Dictionary<Konto, Betrag> Positionen1 = new Dictionary<Konto, Betrag>();
            Konto konto1 = new Konto("1");
            Konto konto2 = new Konto("2");
            Sollbetrag betrag1 = new Sollbetrag(5);
            Habenbetrag betrag2 = new Habenbetrag(5);
            Positionen1.Add(konto1, betrag1);
            Positionen1.Add(konto2, betrag2);
            Buchung buchung1 = new Buchung(Positionen1);

            Dictionary<Konto, Betrag> Positionen2 = new Dictionary<Konto, Betrag>();
            Konto konto3 = new Konto("3");
            Konto konto4 = new Konto("4");
            Sollbetrag betrag3 = new Sollbetrag(5);
            Habenbetrag betrag4 = new Habenbetrag(5);
            Positionen2.Add(konto3, betrag3);
            Positionen2.Add(konto4, betrag4);
            Buchung buchung2 = new Buchung(Positionen2);

            List<Buchung> buchungen = new List<Buchung>();
            buchungen.Add(buchung1);
            buchungen.Add(buchung2);
            Buchungsstapel buchungsstapel = Buchungsstapel.ErstelleBuchungsstapel(buchungen);
            Assert.IsNotNull(buchungsstapel);
        }
    }
}
