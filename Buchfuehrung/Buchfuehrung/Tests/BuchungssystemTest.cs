﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buchfuehrung.DOMAIN;
using NUnit.Framework;

namespace Buchfuehrung.Tests
{
    [TestFixture]
    public class BuchungssystemTest
    {
        [Test]
        public void OnNonExistantKonto_Kontostand_ReturnsBetragNull()
        {
            Buchungssystem bs = new Buchungssystem();
            var expectedResult = Betrag.Ausgeglichen();
            var resultBetrag = bs.Kontostand(new Konto("123456"));
            Assert.AreEqual(expectedResult.GetBetragInCent(), resultBetrag.GetBetragInCent());
        }

        [Test]
        public void BestehendesKontoMitBuchungUndBetragGroesserNull_Kontostand_GibtBetragGroesserNull()
        {
            Buchungssystem bs = new Buchungssystem();
            Konto konto = new Konto("123456");
            Konto konto2 = new Konto("223456");
            Betrag betrag = Betrag.SollInCent(50);
            Betrag betrag2 = Betrag.HabenInCent(50);

            Dictionary<Konto, Betrag> dict = new Dictionary<Konto, Betrag>();

            dict.Add(konto, betrag);
            dict.Add(konto2, betrag2);
            Buchung buchung = Buchung.LegeBuchungAn(dict);

            bs.AddBuchung(buchung);

            Betrag actualBetrag = bs.Kontostand(konto);
            Assert.AreEqual(actualBetrag, betrag);
            Assert.AreEqual(bs.Kontostand(konto2), betrag2);
        }
    }
}