﻿using Buchfuehrung.DOMAIN;
using NUnit.Framework;

namespace Buchfuehrung.Tests
{
    [TestFixture]
        public class SaldierenTests
        {
            [Test]
            public void NullBetragTest()
            {
            Betrag nullBetrag = Betrag.Ausgeglichen();
            Betrag sollBetrag = Betrag.SollInCent(15);
            Betrag habenBetrag = Betrag.HabenInCent(42);
            
            Betrag saldo = Saldieren.saldiere(nullBetrag, sollBetrag);
            Betrag saldo2 = Saldieren.saldiere(sollBetrag, nullBetrag);

            Assert.IsTrue(saldo is Sollbetrag);
            Assert.AreEqual(saldo, sollBetrag);

            Assert.IsTrue(saldo2 is Sollbetrag);
            Assert.AreEqual(sollBetrag, saldo2);

            saldo = Saldieren.saldiere(nullBetrag, habenBetrag);
            saldo2 = Saldieren.saldiere(habenBetrag, nullBetrag);

            Assert.IsTrue(saldo is Habenbetrag);
            Assert.AreEqual(saldo, habenBetrag);

            Assert.IsTrue(saldo2 is Habenbetrag);
            Assert.AreEqual(habenBetrag, saldo2);

            saldo = Saldieren.saldiere(nullBetrag, nullBetrag);
            Assert.IsTrue(saldo is AusgeglichenerBetrag);
            Assert.AreEqual(saldo, nullBetrag);

        }

        [Test]
        public void AddSollTest()
        {
            Betrag sollBetrag0 = Betrag.SollInCent(42);
            Betrag sollBetrag1 = Betrag.SollInCent(58);
            Betrag saldo = Saldieren.saldiere(sollBetrag0, sollBetrag1);

            Assert.IsTrue(saldo is Sollbetrag);
            Assert.AreEqual(100, saldo.GetBetragInCent());
        }

        [Test]
        public void AddHabenTest()
        {
            Betrag habenBetrag0 = Betrag.HabenInCent(42);
            Betrag habenBetrag1 = Betrag.HabenInCent(58);
            Betrag saldo = Saldieren.saldiere(habenBetrag0, habenBetrag1);

            Assert.IsTrue(saldo is Habenbetrag);
            Assert.AreEqual(100, saldo.GetBetragInCent());
        }

        [Test]
        public void SaldoSollHabenTest()
        {
            Betrag sollBetrag = Betrag.SollInCent(42);
            Betrag habenBetrag = Betrag.HabenInCent(10);

            Betrag saldo = Saldieren.saldiere(sollBetrag, habenBetrag);

            Assert.IsTrue(saldo is Sollbetrag);
            Assert.AreEqual(32, saldo.GetBetragInCent());

            saldo = Saldieren.saldiere(habenBetrag, sollBetrag);

            Assert.IsTrue(saldo is Sollbetrag);
            Assert.AreEqual(32, saldo.GetBetragInCent());

            sollBetrag = Betrag.SollInCent(10);
            habenBetrag = Betrag.HabenInCent(42);
            saldo = Saldieren.saldiere(sollBetrag, habenBetrag);

            Assert.IsTrue(saldo is Habenbetrag);
            Assert.AreEqual(32, saldo.GetBetragInCent());

            saldo = Saldieren.saldiere(habenBetrag, sollBetrag);

            Assert.IsTrue(saldo is Habenbetrag);
            Assert.AreEqual(32, saldo.GetBetragInCent());

            sollBetrag = Betrag.SollInCent(42);
            habenBetrag = Betrag.HabenInCent(42);
            saldo = Saldieren.saldiere(sollBetrag, habenBetrag);

            Assert.IsTrue(saldo is AusgeglichenerBetrag);
        }
    }
    
}
;