﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buchfuehrung.Adapter
{
    interface IBuchungssystemErstellungUI
    {
        string readNameBuchungssystem();
        string readNameKontorahmen();
        void printMessage(string message);

    }
}
