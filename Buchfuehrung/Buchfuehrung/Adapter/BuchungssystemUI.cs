﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Buchfuehrung.Anwendungslayer;

namespace Buchfuehrung.Adapter
{
    class BuchungssystemUIPrimitive : IBuchungssystemUI
    {
        private string lastMessage;
        private Dictionary<string, string> readStrings = new Dictionary<string, string>();

        public void printMessage(string message)
        {
            lastMessage = message;
        }

        public string readString(string type)
        {
            string str = readStrings[type];
            if (null == str)
            {
                return "";
            } else
            {
                return str;
            }
        }

        public string getLastMessage()
        {
            return lastMessage;
        }

        public void setString(string type, string str)
        {
            readStrings[type] = str;
        }
    }
}
