﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buchfuehrung.Adapter
{
    class BuchungssystemErstellungUI:IBuchungssystemErstellungUI
    {
        private string lastMessage;
        private string nameBuchungssystem;
        private string nameKontorahmen;
        public string readNameBuchungssystem()
        {
            return nameBuchungssystem;
        }
        public void setNameBuchungssystem(string name)
        {
            nameBuchungssystem = name;
        }
        public string readNameKontorahmen()
        {
            return nameKontorahmen;
        }
        public void setNameKontorahmen(string name)
        {
            nameKontorahmen = name;
        }
        public void printMessage(string message)
        {
            lastMessage = message;
        }
        public string getLastMessage()
        {
            return lastMessage;
        }
        

    }
}
