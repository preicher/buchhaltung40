﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buchfuehrung.Adapter
{
    interface IBuchungBuchungsstapelUI
    {
        string readFileName();

        void printMessage(string message);
    }
}
