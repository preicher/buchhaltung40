﻿using System;
using Buchfuehrung.DOMAIN;
using Buchfuehrung.Ports;

namespace Buchfuehrung.Anwendungslayer
{
    internal class BuchungBuchungsStapel
    {
        private Buchungssystem buchungsSystem;
        private IBuchungBuchungsStapelRepoPort buchungBuchungsStapelRepoPort;

        public BuchungBuchungsStapel(IBuchungBuchungsStapelRepoPort buchungBuchungsStapelRepoPort)
        {
            this.buchungBuchungsStapelRepoPort = buchungBuchungsStapelRepoPort;
            this.buchungsSystem = buchungBuchungsStapelRepoPort.LoadBuchungsSystem("testName");
        }

        public Buchungsstapel CreateBuchungsStapel(string buchungsStapelName)
        {
            throw new NotImplementedException();
        }

        public Buchungsstapel LoadBuchungsStapel(string buchungsStapelName)
        {
            throw new NotImplementedException();
        }

        public void SaveBuchungsStapel(string buchungsStapelName)
        {
            throw new NotImplementedException();
        }

        public bool BucheBuchungsStapel(string buchungsStapelName, out string message)
        {
            throw new NotImplementedException();
        }
    }
}