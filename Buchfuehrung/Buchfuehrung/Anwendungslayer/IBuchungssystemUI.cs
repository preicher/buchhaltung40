﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buchfuehrung.Anwendungslayer
{
    interface IBuchungssystemUI
    {

        string readString(string type);

        void printMessage(string message);
    }
}
