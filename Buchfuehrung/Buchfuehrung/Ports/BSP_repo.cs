﻿using System;
using System.IO;
using Buchfuehrung.DOMAIN;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

namespace Buchfuehrung.Ports
{
    class BSP_repo
    {
        List<Buchungsstapel> ListeVonBuchungsstapeln;
        public void FuegeBuchungsstapelhinzu(Buchungsstapel _bsp)
        {
            ListeVonBuchungsstapeln.Add(_bsp);
        }
        public void SpeichereBuchungsstapel()
        {
            if (ListeVonBuchungsstapeln.Count == 0)
            {
                throw new ArgumentNullException();
            }
            else
            {
                Stream streamWrite = File.Create("Buchungssystem.bin");
                BinaryFormatter binaryWrite = new BinaryFormatter();
                binaryWrite.Serialize(streamWrite, ListeVonBuchungsstapeln);
                streamWrite.Close();
            }
        }

        public Buchungsstapel LadeBuchungsstapel(string filename)
        {
            Stream streamRead = File.OpenRead(filename);
            BinaryFormatter binaryRead = new BinaryFormatter();
            Buchungsstapel _buchungsstapel = (Buchungsstapel)binaryRead.Deserialize(streamRead);
            streamRead.Close();

            return _buchungsstapel;
        }
    }
}
