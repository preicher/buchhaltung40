﻿using Buchfuehrung.DOMAIN;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buchfuehrung.Ports
{
    internal interface IBuchungBuchungsStapelRepoPort
    {
        Buchungssystem LoadBuchungsSystem(string buchungsSystemName);

        Buchungsstapel LoadBuchungsStapel(string buchungsStapelName);
        void SaveBuchungsStapel(string buchungsStapelName);
    }
}
