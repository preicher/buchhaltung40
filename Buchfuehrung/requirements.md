## Buchungssysteme (Fachlogik)

Ein **Buchungssystem** hält die Kontostände zu **Konten** fest. Jeder Kontostand ist ein **Betrag**

Folgendes ist sicherzustellen, um die Grundsätze ordnungsgemäßer Buchhaltung sicherzustellen:
* Das Buchungssystem ist **jederzeit ausgeglichen**, d.h. der Saldo über alle Kontostände ist ein ausgeglichener Betrag
* Das Buchungssystem kann **nur durch Zufügen von *Buchungssätze*n verändert** werden
* Ein Buchungssatz enthält Angaben zu einem Geschäftsvorfall sowie **2-5 Konten mit den darauf zu buchenden Beträgen**. Jeder Buchungssatz muss ausgeglichen sein (**"keine Buchung ohne Gegenbuchung"**)
* Die Buchungssätze müssen **chronologisch, lückenlos und unveränderlich festgehalten** werden. Sie bilden das **Grundbuch(aka Journal)** des Buchungssystems
* Die **Implementierung muss zwingend sicherstellen**, dass inkonsistente Zustände weder durch absichtliche oder versehentliche Fehler im Anwendungscode noch durch Fehleingaben an Schnittstellen erzeugt werden können
* Der **Ausgangskontostand eines noch nicht bebuchten Kontos** ist ausgeglichen


## Beträge und Saldieren (Fachlogik)
Das System arbeitet durchgehend mit **Beträgen**. 
* Ein **Betrag** kann eine der Formen sein:
  * ein **Sollbetrag** in Cent
  * ein **Habenbetrag** in Cent
  * ein **ausgeglichener** Betrag (gleichbedeutend mit 0 Cent)
* Um **Rundungsfehler zu verwenden** sind intern nur **Cent als Ganzzahlwerte** zu verwenden. Eine Umwandlung in eine Nachkommastelle an der benutzeroberfälche bleibt unbenommen

**Saldieren** von Beträgen ergibt immer einen neuen Betrag mit folgenden Regeln:
* Ausgeglichen, wenn die Summe aller Sollbeträge der Summe aller Habenbeträge entspricht
* Sollbetrag oder Habenbetrag mit der (positiven) Differenz der Summen von Soll- und Habenbeträgen, je nachdem welche Summe größer ist
* Ausgeglichene Beträge haben keinen Einfluss auf die Saldierung 


## Anlegen eines Buchungssystems (Anwendungsfall)
Als Buchhalter muss ich ein neues Buchungssystem anlegen können, um darin die Buchhaltung eines Mandanten zu führen.

Abnahmekriterien:
- Ich kann den Namen der Buchungssystems angeben. Das System lässt nur noch nicht verwendete Namen zu
- Ich kann den Namen des zu verwendenden Kontenrahmens angeben
- Nach dem erfolgreichen Anlegen eines Buchungssystems kann ich dieses zum Buchen verwenden
- Alle Konten des neuen Buchungssystems sind zu Beginn ausgeglichen

## Erfassen von Buchungsstapeln (Anwendungsfall)
Als Buchhalter arbeite ich in Buchungsstapeln, die mir ermöglichen, eine Reihe kontierter Belege zu erfassen und zu überprüfen, bevor ich die Buchung auslöse.

Abnahmekriterien:
- Ich kann einen neuen Buchungsstapel anlegen
- Ich kann die Arbeit an einem bestehenden, zwischengespeicherten Bchungsstapel fortsetzen
- Ich kann einem Buchungsstapel neue Buchungssätze/Positionen zufügen
- Ich kann bestehende Positionen des buchungsstapels korrigieren
- Das System zeigt mir die Soll-Summe, die Haben-Summe und den Saldo des Stapels an
- Das System zeigt mir die Soll-Summe, die Haben-Summe und den Saldo ausgewählter Konten des Stapels an
  
## Buchen von Buchungsstapeln
Als Buchhalter möchte ich fertige Buchungsstapel buchen können.

Abnahmekriterien:
- Das System stellt sicher, dass der Buchungsstapel konsistent, i.e. ausgeglichen ist
- Das System bucht den Buchungsstapel in das Buchungssystem ein, indem jede Position zu einem Buchungssatz im Grundbuch wird
- Nach der abgeschlossenen Buchung kann der Buchungsstapel nicht mehr verwendet werden
- Das System stell sicher, dass der Buchungsstapel ganz oder gar nicht eingebucht wurde
  